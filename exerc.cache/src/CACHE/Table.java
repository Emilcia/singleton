package CACHE;

public class Table {
	
	private int Id;
	private int EnumId;
	private String Code;
	private String value;
	private String EnumerationName;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public int getEnumId() {
		return EnumId;
	}
	public void setEnumId(int enumId) {
		EnumId = enumId;
	}
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getEnumerationName() {
		return EnumerationName;
	}
	public void setEnumerationName(String enumerationName) {
		EnumerationName = enumerationName;
	}
	

}
