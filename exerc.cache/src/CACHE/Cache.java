package CACHE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cache {

	Map<String, ArrayList<Table>> cacheObjects = new HashMap<String, ArrayList<Table>>();
	
	public void Add(Table x)
	{
		cacheObjects.get(x.getEnumerationName()).add(x);
	}
	public ArrayList<Table> get(String key)
	{
		return cacheObjects.get(key);
	}
	
	
	public void Clean()
	{
		for(String x : cacheObjects.keySet())
		{
			cacheObjects.remove(x);
		}
	}
	private Cache(){}
	
	private static Cache singl;
	private static Object token = new Object();
	
	public static Cache getInstance()
	{
		if(singl == null){
			synchronized(token)
			{
				if(singl==null)
					singl = new Cache();
			}
		}
		return singl;
	}

}
